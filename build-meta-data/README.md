# build-meta-data.xml

build-meta-data.xml provides various meta-data from the build of your package
and is visible in NSO using `show packages`.

For example:
```
admin@ncs> show packages package bgworker 
packages package bgworker
 package-version 1.0
 description     "A library for running Python background processes in Cisco NSO"
 ncs-min-version [ 4.5 ]
 directory       /nso/run/state/packages-in-use/1/bgworker
 build-info date 2020-05-19T13:05:56+00:00
 build-info file bgworker/package:5.3-kll
 build-info arch linux.x86_64
 build-info python "Python 3.7.3"
 build-info package name bgworker
 build-info package version 1.0
 build-info package ref heads/master-0-g59344c1-dirty
 build-info package sha1 59344c1d72522b5f4954ee32ff63098ada28ac7b
 build-info ncs version 5.3
 oper-status up
[ok][2020-05-19 13:08:59]
admin@ncs> 
```

## Usage

Producing the build-meta-data.xml file requires bash and git to be installed.

Copy the two files `build-meta-data.xml.in` and `bmd.mk` to the `src/`
directory in your package. Include `bmd.mk` in your Makefile and add
`../build-meta-data.xml` as a dependency of some target, for example the `all`
target.

It could look like this:
```
include bmd.mk

all: fxs other_target ../build-meta-data.xml

```

Add `build-meta-data.xml` to the `.gitignore` in the root of your package repo.
We do not want the generate `build-meta-data.xml` file to be accidentally
committed to git - it should always be produced on build!
