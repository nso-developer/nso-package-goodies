# pylint
Enable pylint for your by placing `pylint.mk` and `.pylintrc` in the `src/`
directory of your package. Then include `pylint.mk` in your `Makefile` and
depend on the `pylint` target, for example:

```
all: fxs pylint

include pylint.mk
```

**PLACE THE INCLUDE AFTER the `all` TARGET**. The first target in your
`Makefile` becomes the default target and if you place `include pylint.mk` at
the top of the file, the first target in `pylint.mk` will become the default
target, which in turn might break other things.
